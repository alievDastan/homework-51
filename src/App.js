import React from "react";
import './App.css';
import Number from "./Number/Number";

class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            min: 5,
            max: 36,
        }
    }

    generateNumber = (min, max) => {

        let m = {};
        let a = [];
        for ( let i = 0; i < 5; i++) {
            let rand = Math.floor(Math.random() * (max - 1));
            a.push(((rand in m) ? m[rand] : rand) + 1);
            let l = max - i - 1;
            m[rand] = (l in m) ? m[l] : l;
        }
        a.sort(function (a,b) {
            return a - b;
        });
        return a;
    }

    getInputs = () => {
        const someNum = this.generateNumber(this.state.min, this.state.max);
        this.setState({
            number1: someNum[0],
            number2: someNum[1],
            number3: someNum[2],
            number4: someNum[3],
            number5: someNum[4]
        });
    }

    render() {
        return (
            <div className="content">
                <Number randomNum = {this.state.number1}></Number>
                <Number randomNum = {this.state.number2}></Number>
                <Number randomNum = {this.state.number3}></Number>
                <Number randomNum = {this.state.number4}></Number>
                <Number randomNum = {this.state.number5}></Number>

                <button onClick={ this.getInputs } className="generate">Generate</button>
            </div>
        );
    }

}

export default App;

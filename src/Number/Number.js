import React from "react";

const Number = props => {
    return(
        <div className="number">
            <span>{props.randomNum}</span>
        </div>
    );
};

export default Number;
